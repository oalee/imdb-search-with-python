from finder import *
from dbhelper import DbHelper
import argparse
import sys
import os

logging.basicConfig(level=logging.INFO)


class ReadableDir(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        for prospective_dir in values:
            if not os.path.isdir(prospective_dir):
                raise argparse.ArgumentTypeError("readable_dir:{0} is not a valid path".format(prospective_dir))
            if os.access(prospective_dir, os.R_OK):
                pass
            else:
                raise argparse.ArgumentTypeError("readable_dir:{0} is not a readable dir".format(prospective_dir))
        setattr(namespace, self.dest, values)


def init_parser():
    parser = argparse.ArgumentParser(description="Searching movies detail in terminal")
    parser.add_argument('--version', action='version', version='%(prog)s 1.0')
    parser.add_argument("--dirs", nargs="*", action=ReadableDir, default=os.getcwd())
    parser.add_argument("--name", nargs="*")
    parser.add_argument('-o', dest="output", nargs='?', type=argparse.FileType('w'), default=sys.stdout)
    return parser.parse_args(sys.argv[1:])


def search_in_dir(dirs, out):
    movies_list = []
    for d in dirs:
        print(d)
    for directory in dirs:
        for x in Path(directory).iterdir():
            print(x)
        movies_list += [(str(x).split("/")[-1], str(x), du(str(x))) for x in Path(directory).iterdir()]

    for item in movies_list:
        logging.info(item)

    writer_file = out
    maps = []
    temp_file_path = os.environ['HOME'] + "/.imdb_temp/"
    if not os.path.exists(temp_file_path):
        os.makedirs(temp_file_path)
        os.system("touch " + str(temp_file_path) + "raw_file")
    temp_file_path += "movies.db"
    db = DbHelper(temp_file_path)

    for item in movies_list:
        if db.item_exists(item[1]):
            # todo print movies
            continue
        movie = try_to_find_movie(correct_name(item[0]))
        if movie is None or movie['title'] is "":
            logging.info("could not find movie by name " + correct_name(item[0]))
            continue
        movie['size'] = str(item[2])
        movie['dir'] = item[1]
        if 'rating' not in movie:
            movie['rating'] = 0
        logging.info('got the detail of movie , map is ' + str(movie))
        db.insert(movie)

    for item in db.get_all():
        print(item)


def main():
    parser = init_parser()
    print(parser)
    if parser.name is not None:
        for movie in parser.name:
            print_to_file(parser.output, try_to_find_movie(correct_name(movie)))
        sys.exit()
    search_in_dir(parser.dirs, parser.output)


if __name__ == '__main__':
    main()

# while 1:
#     pass

# dirs = ['/run/media/al/Data III/Movies', '/run/media/al/Data/MoviE', '/run/media/al/Data/MoviE/2012',
#         '/run/media/al/Data/MoviE/2011 Movies', '/run/media/al/Data/Movies',
#         '/run/media/al/Data II/Movies', '/run/media/al/Data II/Movies_2', '/run/media/al/Data II/Movies 2015',
#         '/run/media/al/Data IV/Movies']
