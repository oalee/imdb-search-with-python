import sqlite3


class DbHelper:
    def __init__(self, dbpath):
        self.dbpath = dbpath
        self.conn = sqlite3.connect(dbpath)
        self.table_name = "movies"
        self.cursor = self.create_table()

    def create_table(self):
        create_query = 'create table if not exists ' + self.table_name + \
                       ' (id integer primary key AUTOINCREMENT ' + \
                       ', title text, metascore real, genre text ,\
                        storyline text , dir text , size int , ttl text);'
        cursor = self.conn.cursor()
        cursor.execute(create_query)
        self.conn.commit()
        return cursor

    def insert(self, values):
        item = (values['title'], values['metascore'], values['genre'], values['storyline'],\
                values['dir'], values['size'], values['ttl'])
        insert_query = 'insert into ' + self.table_name + \
                       ' (title, metascore, genre, storyline, dir, size, ttl)' \
                       + ' values ( ?,?,?,?,?,?,? );'
        self.cursor.execute(insert_query, item)
        self.conn.commit()

    def get_all(self):
        select_query = "select * from " + self.table_name + " ;"
        self.cursor.execute(select_query)
        return self.cursor.fetchall()

    def item_exists(self, directory):
        select_query = "select * from " + self.table_name + " where dir = ? ;"
        self.cursor.execute(select_query, (directory,))
        return self.cursor.fetchone() is not None
